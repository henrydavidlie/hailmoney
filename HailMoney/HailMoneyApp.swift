//
//  HailMoneyApp.swift
//  HailMoney
//
//  Created by Henry David Lie on 31/05/21.
//

import SwiftUI

@main
struct HailMoneyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
